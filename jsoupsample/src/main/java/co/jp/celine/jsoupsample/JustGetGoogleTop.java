package co.jp.celine.jsoupsample;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class JustGetGoogleTop {
	public static void main(String[] args) {
		try {
			Document document = Jsoup.connect("http://google.co.jp/").get();
			System.out.println(document.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
