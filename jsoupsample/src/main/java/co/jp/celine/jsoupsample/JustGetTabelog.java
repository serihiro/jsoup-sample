package co.jp.celine.jsoupsample;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class JustGetTabelog {

	/**
	 * Ajaxだから取れない(´Д⊂ｸﾞｽﾝ
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getTabelogTochigi() throws IOException {
		String tabelogTochigiUrl = "http://tabelog.com/tochigi/0/0/lst/?vs=1&sk=&cid=top_navi1&sw=&LstCosT=0&SrtT=trend&sa=%E6%A0%83%E6%9C%A8%E7%9C%8C";
		String result = "";
		Document doc = Jsoup.connect(tabelogTochigiUrl).get();
		result = doc.getElementById("column-main")
				.getElementById("targeting_restaurants").toString();
		return result;
	}
}
